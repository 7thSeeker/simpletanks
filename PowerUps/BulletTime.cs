﻿using System.Linq;

//in fact, this class is some kind of "destroy every brick" bonus
//do not be confused by it's name
public class BulletTime : Powerup
{
    public override void Use(Tank tank)
    {
        FindObjectsOfType<Entity>().Where(x => x.data.id == 4).ToList().ForEach(x => x.InstantKill());

        Destroy(gameObject);
    }
}
