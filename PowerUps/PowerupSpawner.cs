﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(PowerupSpawner))]
public class PowerupSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var ps = (PowerupSpawner)target;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Spawn interval: ", GUILayout.Width(115));
        EditorGUILayout.MinMaxSlider(ref ps.intervalMin, ref ps.intervalMax, 1, 60);
        EditorGUILayout.EndHorizontal();
    }
}
#endif

public class PowerupSpawner : MonoBehaviour 
{
    public Powerup[] powerups;

    public float intervalMin;
    public float intervalMax;

    Powerup spawned;

    IEnumerator Start()
    {
        WaitForEndOfFrame skipFrame = new WaitForEndOfFrame();

        while(true)
        {
            var nextSpawnDelay = Random.Range(intervalMin, intervalMax);

            while(spawned != null)
            {
                yield return skipFrame;
            }

            yield return new WaitForSeconds(nextSpawnDelay);

            var pos = new Vector2();

            pos.x = Random.Range(0, LevelManager.instance.data.mapSize);
            pos.y = Random.Range(0, LevelManager.instance.data.mapSize);

            spawned = ((GameObject)Instantiate(powerups.GetRandomElement().gameObject, pos, Quaternion.identity)).GetComponent<Powerup>();
        }
    }
}
