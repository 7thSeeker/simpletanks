﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public abstract class Powerup : MonoBehaviour
{
    public float duration;

    public abstract void Use(Tank tank);
}
