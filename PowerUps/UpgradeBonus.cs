﻿using UnityEngine;

public class UpgradeBonus : Powerup
{
    public override void Use(Tank tank)
    {
        tank.UpgradeEffect();

        Destroy(gameObject);
    }
}
