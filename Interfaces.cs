﻿using UnityEngine;
using System.Collections.Generic;

interface IListedCache
{
    List<Object> cache { get; set; }
    void InitializeCache();
    void ClearCache();
    void AddItemToCache(Object item);
    void RemoveItemFromCache(Object item);
}
