﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public sealed class MainMenu : MonoBehaviourSingleton<MainMenu>
{
    public GameObject mapChosingPanel;
    public GameObject mapChosingPanelContent;
    public GameObject mapChosingButtonPrefab;
    public Text noMapText;
    List<GameObject> cache;

    void Start()
    {
        mapChosingPanel.SetActive(false);
        LevelData.CreateLevelDataDirectory();
    }

    public void LaunchGame()
    {
        cache = new List<GameObject>();

        mapChosingPanel.SetActive(true);

        var names = LevelData.GetAllLevelNames();

        int i = 0;

        foreach(var lvl in names)
        {
            var btn = Instantiate(mapChosingButtonPrefab.gameObject);
            btn.transform.SetParent(mapChosingPanelContent.transform);
            var btn_text = btn.GetComponentInChildren<Text>();
            btn_text.text = lvl;
            btn.GetComponent<Button>().onClick.AddListener(() => LoadGameScene(btn_text));
            cache.Add(btn);

            i++;
        }

        foreach(var lvl in LevelData.GetAllEditorCreatedLevels())
        {
            var btn = Instantiate(mapChosingButtonPrefab.gameObject);
            btn.transform.SetParent(mapChosingPanelContent.transform);
            var btn_text = btn.GetComponentInChildren<Text>();
            btn_text.text = lvl.name;
            var tmp_lvl = Resources.Load<LevelDataSerialized>("LevelData/" + lvl.name);
            btn.GetComponent<Button>().onClick.AddListener(() => LoadGameSceneFromAsset(tmp_lvl));
            cache.Add(btn);

            i++;
        }

        noMapText.gameObject.SetActive(i <= 0);

    }

    public void LoadGameSceneFromAsset(LevelDataSerialized asset)
    {
        LevelData.activeLevelData = asset.ToLevelData();
        SceneManager.LoadScene("game_scene");
    }

    public void LoadGameScene(Text nameText)
    {
        LevelData.activeLevelData = LevelData.LoadFromDisk(nameText.text);
        SceneManager.LoadScene("game_scene");
    }

    public void CancelLaunch()
    {
        if (!mapChosingPanel.activeInHierarchy)
            return;

        cache.ForEach(x => Destroy(x));
        cache = new List<GameObject>();
        mapChosingPanel.SetActive(false);
    }

    public void LoadLevelConstructionScene()
    {
        SceneManager.LoadScene("level_editor");
    }
}
