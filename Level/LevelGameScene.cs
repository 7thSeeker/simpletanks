﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class LevelGameScene : LevelManager
{
    public const string
        VICTORY_MESSAGE = "Victory!",
        DEFEAT_MESSAGE = "Defeat...";

    public readonly int[]
        enemyEntitesIDs = new int[] { 1, 2 };

    public BoxCollider2D mapBound;
    public GameObject playerTankPrefab;
    public GameObject gameOverPanel;
    public Text gameOverMessage;
    public Text enemiesCounterText;

    int enemiesLeft;

    bool gameOverPending;

    void Start()
    {
        Instantiate(playerTankPrefab, spawnPosition, Quaternion.identity);

        enemiesLeft = FindObjectsOfType<Entity>().Where(x => enemyEntitesIDs.Contains(x.data.id)).Count();

        InstantiateBounds();
    }

    void OnEnable()
    {
        Entity.OnDead += Entity_OnDead;
    }

    void OnDisable()
    {
        Entity.OnDead -= Entity_OnDead;
    }

    private void Entity_OnDead(int entityID)
    {
        if (entityID == Tank.instance.data.id)
        {
            StartCoroutine(OpenGameOverPanel(DEFEAT_MESSAGE));
        }
        else if (enemyEntitesIDs.Any(x => x == entityID))
        {
            enemiesLeft--;
        }
    }

    void Update()
    {
        enemiesCounterText.text = string.Format("Enemies left: {0}", enemiesLeft);

        if (!gameOverPending && enemiesLeft <= 0)
        {
            StartCoroutine(OpenGameOverPanel(VICTORY_MESSAGE));
        }
    }

    public IEnumerator OpenGameOverPanel(string message)
    {
        if (gameOverPending)
            yield break;

        gameOverPending = true;

        yield return new WaitForSeconds(2);

        gameOverPanel.SetActive(true);

        gameOverMessage.text = message;
    }


    void InstantiateBounds()
    {
        var size = data.mapSize;

        var left = (GameObject)Instantiate(mapBound.gameObject, new Vector3(-.5F, size / 2), Quaternion.identity);
        left.GetComponent<BoxCollider2D>().size = new Vector2(1, size);

        var right = (GameObject)Instantiate(mapBound.gameObject, new Vector3(size + .5F, size / 2), Quaternion.identity);
        right.GetComponent<BoxCollider2D>().size = new Vector2(1, size);

        var top = (GameObject)Instantiate(mapBound.gameObject, new Vector3(size / 2, size + .5F), Quaternion.identity);
        top.GetComponent<BoxCollider2D>().size = new Vector2(size, 1);

        var down = (GameObject)Instantiate(mapBound.gameObject, new Vector3(size / 2, -.5F), Quaternion.identity);
        down.GetComponent<BoxCollider2D>().size = new Vector2(size, 1);
    }

}
