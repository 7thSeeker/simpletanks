﻿using UnityEngine.UI;

public class LevelEditor : LevelManager
{
    public InputField levelName;

    public void SaveCurrentLevel()
    {
        var l_name = levelName.text == string.Empty ? "untitled" : levelName.text;
        data.SaveToDisk(l_name);
    }

    public override void LoadLevel()
    {
        data = new LevelData(12);

        data.map[data.mapSize / 2] = TileEditor.SPAWN_ID;

        InitializeCache();

        Rebuild();
    }
}
