﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviourSingleton<LevelManager>, IListedCache
{
    public LevelData data { get; protected set; }

    public List<Object> cache { get; set; }

    public Vector3 spawnPosition
    {
        get { return FindObjectsOfType<TileEditor>().First(x => x.id == TileEditor.SPAWN_ID).transform.position; }
    }


    public void Awake()
    {
        LoadLevel();
    }

    public virtual void LoadLevel()
    {     
        ClearCache();

        data = LevelData.activeLevelData;

        Rebuild();
    }

    public void Exit()
    {
        SceneManager.LoadScene("main_menu");
    }

    public void Rebuild()
    {
        int i = 0;
        for (int x = 0; x < data.mapSize; x++)
        {
            for (int y = 0; y < data.mapSize; y++)
            {
                AddItemToCache(Instantiate(TilesCollection.GetByID(data.map[i++]), new Vector3(y, x), Quaternion.identity));
            }
        }
    }

    public void InitializeCache()
    {
        cache = new List<Object>();
    }

    public void ClearCache()
    {
        if(cache != null)
        cache.ForEach(x => Destroy(x));

        InitializeCache();
    }

    public void AddItemToCache(Object item)
    {
        cache.Add(item);
    }

    public void RemoveItemFromCache(Object item)
    {
        if (cache.Contains(item))
        {
            cache.Remove(item);
            Destroy(item);
        }
    }
}



