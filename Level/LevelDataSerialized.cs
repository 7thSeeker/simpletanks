﻿using UnityEngine;
using System.Collections;

public class LevelDataSerialized : ScriptableObject
{
    [SerializeField]
    public byte[] map;
    public byte mapSize;

    public LevelData ToLevelData()
    {
        return new LevelData(map, mapSize);
    }

#if UNITY_EDITOR
    public static LevelDataSerialized Create(LevelData data, string name)
    {

        var tmp = CreateInstance<LevelDataSerialized>();

        tmp.mapSize = data.mapSize;

        tmp.map = data.map;

        string path = UnityEditor.EditorUtility.SaveFilePanel("Save construction block", "Assets/Resources/LevelData", name, "asset");
        if (string.IsNullOrEmpty(path))
            return null;

        path = UnityEditor.FileUtil.GetProjectRelativePath(path);

        UnityEditor.AssetDatabase.CreateAsset(tmp, path);
        UnityEditor.EditorUtility.SetDirty(tmp);
        UnityEditor.AssetDatabase.SaveAssets();

        return UnityEditor.AssetDatabase.LoadAssetAtPath<LevelDataSerialized>(path);
    }
#endif
}
