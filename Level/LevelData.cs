﻿using UnityEngine;
using System.IO;

[System.Serializable]
public sealed class LevelData 
{
    public byte[] map;
    public byte mapSize { get; private set; }

    public const string PREMADE_LEVEL_DATA_FOLDER = "/Resources/LevelData";
    public const string FILE_EXTENSION = ".lvl";

    public static LevelData activeLevelData;
    public static string directoryPath { get { return Application.dataPath + PREMADE_LEVEL_DATA_FOLDER + "/"; } }

    public LevelData(byte mapSize)
    {
        map = new byte[mapSize * mapSize];
        this.mapSize = mapSize;
    }

    public void ChangeTileAt(int x, int y, byte newTile)
    {
        int realIndex = y * mapSize + x;

        map[realIndex] = newTile;
    }

    public LevelData(byte[,] mapData, byte mapSize)
    {

        this.mapSize = mapSize;

        this.map = new byte[mapSize * mapSize];

        int i = 0;
        for(int x = 0; x < mapSize; x++)
        {
            for(int y = 0; y < mapSize; y++)
            {
                this.map[i++] = mapData[x, y];
            }
        }

    }

    public LevelData(byte[] mapData, byte mapSize)
    {
        this.map = mapData;
        this.mapSize = mapSize;
    }

    public static void CreateLevelDataDirectory()
    {
        if (!Directory.Exists(directoryPath))
            Directory.CreateDirectory(directoryPath);
    }

    public static LevelDataSerialized[] GetAllEditorCreatedLevels()
    {
        var tmp = Resources.LoadAll<LevelDataSerialized>("LevelData");
        Debug.Log(tmp.Length);
        return tmp;
    }

    public void SaveToDisk(string fileName)
    {
        CreateLevelDataDirectory();

        if (Application.isEditor)
        {
#if UNITY_EDITOR
            LevelDataSerialized.Create(this, fileName);
#endif
        }
        else
        {
            byte[] data = Serialization.ObjectToByteArray(this);

            File.WriteAllBytes(directoryPath + fileName + FILE_EXTENSION, data);
        }

    }

    public static string[] GetAllLevelNames()
    {
        string[] tmp = Directory.GetFileSystemEntries(directoryPath, "*" + FILE_EXTENSION);

        string[] result = new string[tmp.Length];
        for(int i = 0; i < tmp.Length; i++)
        {
            result[i] = Path.GetFileName(tmp[i]);
        }
        return result;
    }

    public static LevelData LoadFromDisk(string activeLevelDataFileName)
    {
        var fPath = directoryPath + activeLevelDataFileName;

        Debug.Log("path: " + fPath);

        if(File.Exists(fPath))
        {

                Debug.Log("load specific level data");
                LevelData data = (LevelData)Serialization.ByteArrayToObject(File.ReadAllBytes(fPath));
                return data;

        }
        else
        {
            return LoadRandomFromDisk();
        }
    }

    public static LevelData LoadRandomFromDisk()
    {
        var files = Directory.GetFiles(directoryPath, "*" + FILE_EXTENSION);

        var fPath = files.GetRandomElement();
        if (File.Exists(fPath))
        {
            Debug.Log("load random level data");
            LevelData data = (LevelData)Serialization.ByteArrayToObject(File.ReadAllBytes(fPath));
            return data;
        }
        else
        {
            //TODO: Need to make this a bit friendly to user, lol
            throw new System.Exception("No suitable map files were found. You need to create one before you can start the game. Use the 'Construction' feature from the 'main_menu' scene.");
        }
    }
}
