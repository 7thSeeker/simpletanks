﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TileEditor : MonoBehaviour
{
    public const int SPAWN_ID = 9;
    public GameObject nextTile;
    public byte id;
    public bool isLevelEditor { get; private set; }

    void Start()
    {
        isLevelEditor = SceneManager.GetActiveScene().name == "level_editor";

        if (!isLevelEditor)
        {
            Destroy(this);
            return;
        }

       // LevelManager.instance.data.map[(int)transform.position.x, (int)transform.position.y] = id;
        LevelManager.instance.data.ChangeTileAt((int)transform.position.x, (int)transform.position.y, id);

        foreach (Transform child in transform)
        {
            var coll = child.GetComponent<Collider2D>();

            if (coll)
                coll.enabled = false;
        }
    }

    void OnMouseDown()
    {
        if (!isLevelEditor || id == SPAWN_ID)
            return;

        Instantiate(nextTile, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
