﻿using UnityEngine;

public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T instance { get { return FindObjectOfType<T>(); } }
}
