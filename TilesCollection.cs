﻿using UnityEngine;
using System.Collections.Generic;

public static class TilesCollection
{
    static Dictionary<byte, GameObject> collection;

    public static void Refresh()
    {
        collection = new Dictionary<byte, GameObject>();
        foreach (var tile in Resources.LoadAll<GameObject>("Prefabs/MapElements"))
        {
            var et = tile.GetComponent<TileEditor>();

            collection.Add(et.id, tile);
        }
    }

    public static GameObject GetByID(byte id)
    {
        if (collection == null)
            Refresh();

        GameObject result;

        if (!collection.TryGetValue(id, out result))
        {
            Debug.LogWarning("No tiles with this ID were found: " + id);
        }

        return result;
    }

}
