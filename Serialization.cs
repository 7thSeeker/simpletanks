﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class Serialization
{
    public static byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
            return null;

        var bf = new BinaryFormatter();
        var ms = new MemoryStream();

        bf.Serialize(ms, obj);

        return ms.ToArray();
    }

    public static object ByteArrayToObject(byte[] buffer)
    {
        var bf = new BinaryFormatter();
        var ms = new MemoryStream();

        ms.Write(buffer, 0, buffer.Length);
        ms.Seek(0, SeekOrigin.Begin);
        var obj = bf.Deserialize(ms);

        return obj;
    }
}