﻿using UnityEngine;
using System.Linq;

public class TurretControlledWeapon : Weapon
{
    public float detectionRange;

    public Transform source;
    public Transform[] directions;

    public Entity thisEntity;

    protected override void Fire(Vector2 source, Vector2 direction)
    {
        base.Fire(source, direction);
    }

    protected override void Update()
    {
        if (thisEntity.isDead)
            return;

        base.Update();

        foreach (var direction in directions)
        {
            RaycastHit2D[] targets = Physics2D.RaycastAll(source.position, direction.position - source.position, detectionRange);

            if (targets.Any(x => x.collider.gameObject.tag == "Player"))
                Fire(source.position, direction.position);
        }

    }
}
