﻿using UnityEngine;

[CreateAssetMenu]
public class WeaponData : ScriptableObject
{
    public GameObject projectile;
    public float fireRate;

}
