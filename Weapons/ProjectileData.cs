﻿using UnityEngine;

[CreateAssetMenu]
public class ProjectileData : ScriptableObject
{
    public GameObject impactEffect;
    public bool negateHeavyArmor;
    public float damage;
    public float speed;
    public float lifeTime;
    public float impactRadius;
}
