﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour, IListedCache
{
    public WeaponData data;
    protected float cooldown;

    public List<UnityEngine.Object> cache { get; set; }

    void Start()
    {
        InitializeCache();
    }

    protected virtual void Fire(Vector2 source, Vector2 direction)
    {
        var src = new FireSource(source, direction);

        Fire(src);
    }

    protected virtual void Fire(params FireSource[] sources)
    {
        if (cooldown > 0)
            return;

        foreach(var src in sources)
        {
            var source = src.source;
            var direction = src.direction;
            var dir = direction - source;
            var angle = -Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            var proj = (GameObject)Instantiate(data.projectile, source, Quaternion.AngleAxis(angle, Vector3.forward));
            AddItemToCache(proj);
            proj.GetComponent<Projectile>().SetOwnerMask(gameObject.layer);
            cooldown = data.fireRate;
        }
    }

    protected virtual void Update()
    {
        cooldown -= Time.deltaTime;
    }

    public void InitializeCache()
    {
        cache = new List<UnityEngine.Object>();
    }

    public void ClearCache()
    {
        cache.ForEach(x => Destroy(x));

        InitializeCache();
    }

    public void AddItemToCache(UnityEngine.Object item)
    {
        if (!cache.Contains(item))
        {
            cache.Add(item);
        }
    }

    public void RemoveItemFromCache(UnityEngine.Object item)
    {
        if (cache.Contains(item))
        {
            cache.Remove(item);
            Destroy(item);
        }
    }
}


public struct FireSource
{
    public Vector2 source;
    public Vector2 direction;

    public FireSource(Transform source, Transform direction)
    {
        this.source = source.position;
        this.direction = direction.position;
    }

    public FireSource(Vector2 source, Vector2 direction)
    {
        this.source = source;
        this.direction = direction;
    }

    public static FireSource[] FromTransforms(Transform[] source, Transform[] direction)
    {
        var sources = new FireSource[source.Length];

        for (int i = 0; i < source.Length; i++)
        {
            sources[i] = new FireSource(source[i], direction[i]);
        }
        return sources;
    }
}