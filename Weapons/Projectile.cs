﻿using UnityEngine;

public class Projectile : MonoBehaviour 
{
    public ProjectileData data;
    public Rigidbody2D rbody;

    LayerMask ownerLayer;

    void Start()
    {
        Destroy(gameObject, data.lifeTime);
    }

    void Update()
    {
        Move();
    }

    void Move()
    {
        rbody.MovePosition(transform.position + transform.up * data.speed * Time.deltaTime);
    }

    public void SetOwnerMask(LayerMask ownerLayer)
    {
        this.ownerLayer = ownerLayer;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        var target = coll.GetComponent<Entity>();
        if (target == null || !target.data.absorbProjectiles || coll.gameObject.layer == ownerLayer)
            return;

        if (data.impactRadius != 0)
            foreach(RaycastHit2D _target in Physics2D.CircleCastAll(transform.position, data.impactRadius, Vector2.zero))
            {           
                if(_target.transform.gameObject.layer != ownerLayer)
                {
                    //TODO: i'll rework this later
                    _target.collider.SendMessage("ApplyDamage", data, SendMessageOptions.DontRequireReceiver);
                }
            }
        else
            if (target.transform.gameObject.layer != ownerLayer)
                target.ApplyDamage(data);

        Destroy(gameObject);
    }

    void OnDestroy()
    {
        if(data.impactEffect)
        {
            Instantiate(data.impactEffect, transform.position, Quaternion.identity);
        }
    }
}
