﻿using UnityEngine;

public class PlayerControlledWeapon : Weapon
{
    public Transform[] source;
    public Transform[] direction;

    protected override void Update()
    {
        if (source == null || direction == null)
            return;

        if (Input.GetKey(KeyCode.Space))
        {
            Fire(FireSource.FromTransforms(source,direction));
        }

        base.Update();


    }
}
