﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Entity : MonoBehaviour
{
    public BoxCollider2D boxCollider { get { return GetComponent<BoxCollider2D>(); } }

    public delegate void Dead(int entityID);
    public static event Dead OnDead;

    public delegate void TakeDamage(ref float amount);
    public event TakeDamage OnTakeDamage;

    public bool isDead
    {
        get
        {
            return hitPoints <= 0;
        }
    }

    public EntityData data;

    public float hitPoints { get; protected set; }

    protected virtual void Start()
    {
        hitPoints = data.hitPoints;
    }

    public void InstantKill()
    {
        hitPoints = 0;

        DeadCheck();
    }

    void DeadCheck()
    {
        if (hitPoints <= 0)
        {
            EntityDeath();
        }
    }

    public void ApplyDamage(ProjectileData data)
    {
        if (isDead || this.data.destructible == false || (this.data.heavyArmored && !data.negateHeavyArmor) == true)
            return;

        if (OnTakeDamage != null)
        {
            OnTakeDamage(ref data.damage);
        }

        hitPoints -= data.damage;


        DeadCheck();
    }

    protected virtual void EntityDeath()
    {
        Destroy(gameObject);
        OnDead(this.data.id);
    }
}
