﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(Tank))]
public class TankEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var t = (Tank)target;

        EditorGUILayout.LabelField("HitPoints: " + t.hitPoints);
    }
}
#endif

public class Tank : Entity
{
    public static Tank instance;
    public int level;
    public GameObject nextTierTankPrefab;
    public GameObject baseTankPrefab;

    public void BulletTimeEffect(float time)
    {
        FindObjectsOfType<Entity>().Where(x => x.data.id == 4).ToList().ForEach(x => x.InstantKill());
    }

    public void UpgradeEffect()
    {
        var t = (GameObject)Instantiate(nextTierTankPrefab, transform.position, Quaternion.identity);
        t.GetComponent<TankControl>().spriteObject.transform.rotation = transform.rotation;
        Destroy(transform.root.gameObject);
    }

    public void Downgrade()
    {
        var t = (GameObject)Instantiate(baseTankPrefab, transform.position, Quaternion.identity);
        t.GetComponent<TankControl>().spriteObject.transform.rotation = transform.rotation;

        Destroy(transform.root.gameObject);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        var powerup = coll.GetComponent<Powerup>();

        if (powerup)
        {
            powerup.Use(this);
        }
    }

    void Awake()
    {
        instance = this;
    }

    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.R))
    //    {
    //        UpgradeEffect();
    //    }
    //}

    protected override void EntityDeath()
    {
        if (level == 0)
        {
            base.EntityDeath();
        }

        else
        {
            Downgrade();
        }
    }

}
