﻿using UnityEngine;

[CreateAssetMenu]
public class EntityData : ScriptableObject
{
    public int id;
    public bool passable;
    public bool destructible;
    public bool heavyArmored;
    public bool absorbProjectiles;
    public float hitPoints = 1;
}
