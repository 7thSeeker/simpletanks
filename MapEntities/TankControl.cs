﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TankControl : MonoBehaviour 
{
    float h { get { return Input.GetAxisRaw("Horizontal"); } }
    float v { get { return Input.GetAxisRaw("Vertical"); } }

    public float speed = 5;

    public GameObject spriteObject;

    Quaternion lookUp { get { return Quaternion.Euler(Vector3.zero); } }
    Quaternion lookLeft { get { return Quaternion.Euler(new Vector3(0, 0, 90)); } }
    Quaternion lookDown { get { return Quaternion.Euler(new Vector3(0, 0, 180)); } }
    Quaternion lookRight { get { return Quaternion.Euler(new Vector3(0, 0, 270)); } }

    void Update()
    {
        if (Tank.instance.isDead)
            return;

        Rotate();
        Move();
    }

    //TODO: do not look below
    void Rotate()
    {
        if (h == 0 && v == 0)
            return;

        Quaternion look = spriteObject.transform.rotation;

        if (h < 0)
            look = lookLeft;

        if (h > 0)
            look = lookRight;

        if (v > 0)
            look = lookUp;

        if (v < 0)
            look = lookDown;

        spriteObject.transform.rotation = look;

    }

    void Move()
    {
        Vector3 velocity = new Vector3();

        velocity.x = v == 0 ? h * speed * Time.deltaTime : 0;
        velocity.y = h == 0 ? v * speed * Time.deltaTime : 0;

        velocity = transform.TransformDirection(velocity);

        transform.Translate(velocity);
    }
}
