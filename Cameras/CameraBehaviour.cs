﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraBehaviour : MonoBehaviour
{
    public Camera cam { get { return GetComponent<Camera>(); } }
}
