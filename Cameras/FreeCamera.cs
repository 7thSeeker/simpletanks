﻿using UnityEngine;

public class FreeCamera : CameraBehaviour
{
    Vector3 camPosition;

    [Range(1, 25)]
    public float scrollSpeed, zoomSpeed;

    public bool freeze;

    void Start()
    {
        var mapSize = LevelManager.instance.data.mapSize / 2;
        cam.transform.position = new Vector3(mapSize, mapSize, -10);
        
        camPosition = cam.transform.position;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            freeze = !freeze;
        }

        if (freeze)
            return;

        var mousePos = cam.ScreenToViewportPoint(Input.mousePosition);

        camPosition.x += Time.deltaTime * (mousePos.x <= .1F ? -scrollSpeed : mousePos.x >= .9F ? scrollSpeed : 0);
        camPosition.y += Time.deltaTime * (mousePos.y <= .1F ? -scrollSpeed : mousePos.y >= .9F ? scrollSpeed : 0);
        cam.orthographicSize += Time.deltaTime * Input.GetAxis("Mouse ScrollWheel") * zoomSpeed * 10;

        camPosition.x = Mathf.Clamp(camPosition.x, 0, LevelManager.instance.data.mapSize);
        camPosition.y = Mathf.Clamp(camPosition.y, 0, LevelManager.instance.data.mapSize);
        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, 4, 10);

        cam.transform.position = camPosition;
    }
}
