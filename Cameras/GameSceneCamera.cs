﻿using UnityEngine;

public class GameSceneCamera : CameraBehaviour
{
    void Start()
    {
        AlignCameraToCenterView();
    }

    void AlignCameraToCenterView()
    {
        var position = new Vector3();
        position.z = -10;

        var mapSize = LevelManager.instance.data.mapSize;

        position.x = mapSize / 2;
        position.y = mapSize / 2; 

        cam.transform.position = position;
    }
}
